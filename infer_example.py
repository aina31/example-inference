#!/usr/bin/env python

import subprocess
import sys
import os
import time
import socket, select

from pylstar.LSTAR import LSTAR
from pylstar.NetworkActiveKnowledgeBase import NetworkActiveKnowledgeBase
from pylstar.Letter import Letter, EmptyLetter
from pylstar.Word import Word
from pylstar.eqtests.RandomWalkMethod import RandomWalkMethod

class serverKnowledgeBase(NetworkActiveKnowledgeBase):

    def __init__(self, executable):
        super(serverKnowledgeBase, self).__init__("127.0.0.1", 4433, timeout=1)
        self.__executable = executable
        self.__sp = None

    def start(self):
        print("Starting server_example")
        server_path = self.__executable
        self.__sp = subprocess.Popen("{}".format(server_path), shell=True)
        time.sleep(1)
        
    def stop(self):
        print("Stoping server")
        if self.__sp is not None:
            print("server machine process is forced to stop")
            self.__sp.terminate()
            self.__sp.kill()

    def submit_word(self, word):

        self._logger.debug("Submiting word '{}' to the network target".format(word))
        print("\n************************** {}".format(word))

        output_letters = []

        s = socket.socket()
        # Reuse the connection
        s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        s.settimeout(self.timeout)
        s.connect((self.target_host, self.target_port))
        try:
            output_letters = [self._submit_letter(s, letter) for letter in word.letters]
        finally:
            s.close()

        return Word(letters=output_letters)

    def _submit_letter(self, s, letter):
        output_letter = EmptyLetter()
        try:
            to_send = ''.join([symbol for symbol in letter.symbols])
            output_letter = Letter(self._send_and_receive(s, self.translate(to_send)))
        except socket.timeout as err:
            output_letter = Letter("Waiting Message")
            self._logger.error(err)
        except Exception as e:
            output_letter = Letter('EOF')
            self._logger.error(e)
        if output_letter == Letter(''):
            output_letter = Letter('EOF')
        print("Output Letter :: {}\n".format(output_letter))
        return output_letter

    def translate(self, msg):
        if msg == "Good_Hello":
            return "HHello55"
        elif msg == "Bad_Hello":
            return "HHello7AA"
        elif msg == "login":
            return "Ltata"
        elif msg == "password":
            return "Psuperpassword"
        else:
            return "Dtest"

def main():
    input_vocabulary = [
	"Good_Hello",
        "Bad_Hello",
	"login",
	"password",
        "data"
    ]
    executable = "python server_example.py"
    serverBase = serverKnowledgeBase(executable)
    try:
        serverBase.start()
        lstar = LSTAR(input_vocabulary, serverBase, max_states = 6)
        server_state_machine = lstar.learn()
    finally:
        serverBase.stop()
        
    dot_code = server_state_machine.build_dot_code()

    output_file = "example.dot"

    with open(output_file, "w") as fd:
        fd.write(dot_code)

    print("==> Server Automata dumped in {}".format(output_file))
    print("Knowledge base stats: {}".format(serverBase.stats))


if __name__ == "__main__":
    main()

