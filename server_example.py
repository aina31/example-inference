import socket

class server:
  def __init__(self, hostname, port, verbose=True, timeout = None):
    self.hostname = hostname
    self.port = port
    self.verbose = verbose
    self.timeout = timeout
    self.database = {'tata': 'superpassword55'}

  def parse(self, msg):
    self.msg_type = msg[0]
    self.content = msg[1:]

  def check_login_password(self):
    try:
      password = self.database[self.login]
      if password == (self.password + self.remain_password):
        return True
    except:
      return False

  def _init_connexion(self):
    if self.verbose:
      print("[+] Listening on %s ... " % (self.hostname))
    self.server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    self.server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    self.server_socket.bind((self.hostname, self.port))
    self.server_socket.listen(5)
    if self.timeout is not None:
        self.server_socket.settimeout(self.timeout)

  def _handle_receive(self, message_type):
    try:
      self.parse(self.socket.recv(1024))
      if self.msg_type != message_type:
        self.socket.send("Unexpected Message")
        self.socket.close()
        return False
      return True
    except:
      self.socket.close()
      return False

  def _send_receive(self):
    self.socket, client_address = self.server_socket.accept()
    self.parse(self.socket.recv(1024))
    if self.msg_type != 'H' or 'Hello' not in self.content:
      self.socket.send("Unexpected Message")
      self.socket.close()
    else:
      self.remain_password = self.content[5:]
      if self._handle_receive('L'):
        self.login = self.content
      if self._handle_receive('P'):
        self.password = self.content
        if self.check_login_password():
          self.socket.send("Connexion Accepted")
          while self._handle_receive('D'):
            self.socket.send("Data OK")
        else:
          self.socket.send("Connexion Rejected")
          self.socket.close()

  def run(self):
    while True:
      self._init_connexion()
      self._send_receive()

def main():
  s = server('127.0.0.1', 4433)
  s.run()
main()

